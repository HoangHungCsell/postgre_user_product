require('dotenv').config();

const express = require('express'),
    app = express(),
    port = process.env.PORT,
    Sequelize = require('sequelize'),
    bodyParser = require('body-parser');

const sequelize = new Sequelize(process.env.DATABASE_URL);


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const employeeRoute = require('./src/api/routes/employee.route'),
    productRoute = require('./src/api/routes/product.route'),
    customerRoute = require('./src/api/routes/customer.route'),
    orderRoute = require('./src/api/routes/order.route');

app.use('/employees', employeeRoute);
app.use('/products', productRoute);
app.use('/customers', customerRoute);
app.use('/orders', orderRoute);

const Customer = require('./src/api/models/customer.model'),
    Order = require('./src/api/models/order.model');

Customer.hasMany(Order, {
    foreignKey: {
        name: 'orders'
    },
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT'
});

app.listen(port);
console.log(`Administration started on: ${port}`)

