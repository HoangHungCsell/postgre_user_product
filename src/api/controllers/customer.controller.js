const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Customer = require('../models/customer.model');
const Order = require('../models/order.model');
const Sequelize = require('sequelize');

exports.register = async (req, res) => {
    try {
        const { name, username, password, birthday, address, phone, gender } = req.body;
        bcryptjs.hash(password, 10, async (err, hash) => {
            if (err)
                res.status(400).send(err);
            else {
                const customer = await Customer.create({
                    name,
                    username,
                    password,
                    birthday,
                    address,
                    phone,
                    gender,
                    hash_password: hash
                });
                return res.json(customer);
            }
        });
    } catch (error) {
        return error;
    }
};

exports.login = async (req, res) => {
    try {
        const customer = await Customer.findOne(
            {
                where: { username: req.body.username }
            }
        )
        if (!customer) return res.json({ message: 'Tên đăng nhập không chính xác' });
        bcryptjs.compare(req.body.password, customer.hash_password, (error, result) => {
            if (error) return res.send(error);
            if (result === true) {
                return res.json({
                    message: "Đăng nhập thành công",
                    token: jwt.sign({
                        id: customer.id,
                        username: customer.username
                    },
                        process.env.JWT_SECRET,
                        { expiresIn: '1d' })
                })
            }
            return res.json({ message: 'Mật khẩu không chính xác' })
        })
    } catch (error) {
        return error;
    }
};

exports.list = async (req, res) => {
    try {
        const customers = await Customer.findAndCountAll({
            where: req.conditions,
            offset: req.query.offset,
            limit: req.query.limit
        });
        return res.json(customers);
    } catch (error) {
        return error;
    }
};

exports.update = async (req, res) => {
    try {
        const { name, username, password, birthday, address, phone, gender, roles } = req.body;
        await Customer.update({
            name,
            username,
            password,
            birthday,
            address,
            phone,
            gender,
            roles
        }, {
            where: {
                id: req.params.id
            }
        });
        const customer = await Customer.findOne({
            where: { id: req.params.id }
        });
        return res.json({
            message: `Đã cập nhật customer 🚀, id: ${req.params.id}`,
            customer
        });
    } catch (error) {
        return error;
    }
};

exports.delete = async (req, res) => {
    try {
        const customer = await Customer.findOne({
            where: { id: req.params.id }
        });
        await customer.destroy();
        return res.json({
            message: `Đã xóa customer 🚀, id: ${req.params.id}`,
            customer
        })
    } catch (error) {
        return error;
    }

};

exports.detail = async (req, res) => {
    try {
        const customer = await Customer.findOne({
            where: { id: req.params.id },
            include: [{
                model: Order,
                as: 'orders',
                through: {
                    attributes: ['name', 'quantity', 'intoMoney'],
                    where: { 'created_by.id': req.data.id }
                }
            }]
        });
        return res.json(customer);
    } catch (error) {
        return error;
    }

};