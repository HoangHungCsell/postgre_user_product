const Product = require('../models/product.model');
const { Sequelize, QueryTypes } = require('sequelize');
const { or, iLike } = Sequelize.Op;

exports.create = async (req, res) => {
    try {
        req.body.created_by = {
            id: req.data.id,
            username: req.data.username
        }
        const product = await Product.create(req.body);
        return res.json(product);
    } catch (error) {
        return error;
    }
};

exports.list = async (req, res) => {
    try {
        const params = req.query ? req.query : {};
        if (params.keyword) {
            const products = await Product.findAndCountAll({
                where: {
                    name: {
                        [iLike]: `%${params.keyword}%`
                    }
                },
                order: [['price', 'DESC']]
            });
            return res.json({ products });
        } else {
            const products = await Product.findAndCountAll();
            return res.json({ products });
        }


    } catch (error) {
        return error;
    }
};

exports.update = async (req, res) => {
    try {
        await Product.update(req.body, {
            where: {
                id: req.params.id
            }
        });
        const product = await Product.findOne({
            where: { id: req.params.id }
        });
        return res.json({
            message: `Đã cập nhật Product 🚀, id: ${req.params.id}`,
            product
        });
    } catch (error) {
        return error;
    }
};

exports.delete = async (req, res) => {
    try {
        const product = await Product.findOne({
            where: { id: req.params.id }
        });
        // await Product.destroy({ where: { id: req.params.id } });
        await Product.destroy();
        return res.json({
            message: `Đã xóa Product 🚀, id: ${req.params.id}`,
            product
        })
    } catch (error) {
        return error;
    }

};

exports.detail = async (req, res) => {
    try {
        const product = await Product.findOne({
            where: { id: req.params.id }
        });
        return res.json(product);
    } catch (error) {
        return error;
    }

};