const Order = require('../models/order.model');
const Customer = require('../models/customer.model');
// const Op = Sequelize.Op;

exports.create = async (req, res) => {
    try {
        const { quantity, price, discount } = req.body;
        req.body.created_by = {
            id: req.data.id,
            username: req.data.username
        }
        req.body.intoMoney = quantity * (price - discount);
        const order = await Order.create(req.body);
        // const customer = await Customer.findOne({
        //     where: { id: req.data.id }
        // });
        // customer.orders.push(order);
        // customer.save();
        // await customer.orders.update(order);
        // Customer.update({ orders: [order] }, {
        //     where: { id: req.data.id }
        // })
        return res.json(order);
    } catch (error) {
        return error;
    }
};

exports.list = async (req, res) => {
    try {
        const orders = await Order.findAndCountAll({
            where: { 'created_by.id': req.data.id }
        });
        return res.json({ orders });
    } catch (error) {
        return error;
    }

};

exports.update = async (req, res) => {
    try {
        const order = await Order.findOne({
            where: { id: req.params.id }
        });
        if (req.body.quantity)
            req.body.intoMoney = req.body.quantity * (order.price - order.discount);
        await order.update(req.body, {
            where: {
                id: req.params.id
            }
        });
        return res.json({
            message: `Đã cập nhật order 🚀, id: ${req.params.id}`,
            order
        });
    } catch (error) {
        return error;
    }
};

exports.delete = async (req, res) => {
    try {
        const order = await Order.findOne({
            where: { id: req.params.id }
        });
        await order.destroy();
        return res.json({
            message: `Đã xóa order 🚀, id: ${req.params.id}`,
            order
        })
    } catch (error) {
        return error;
    }

};

exports.detail = async (req, res) => {
    try {
        const order = await Order.findOne({
            where: { id: req.params.id }
        });
        return res.json(order);
    } catch (error) {
        return error;
    }

};