const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Employee = require('../models/employee.model');
const Sequelize = require('sequelize');

exports.register = async (req, res) => {
    try {
        const { name, username, password, birthday, address, phone, gender, roles } = req.body;
        bcryptjs.hash(password, 10, async (err, hash) => {
            if (err)
                res.status(400).send(err);
            else {
                const employee = await Employee.create({
                    name,
                    username,
                    password,
                    birthday,
                    address,
                    phone,
                    gender,
                    roles,
                    hash_password: hash
                });
                return res.json(employee);
            }
        });
        // const employee = await Employee.create(req.body);
        //     return res.json(employee);
    } catch (error) {
        return error;
    }
};

exports.login = async (req, res) => {
    try {
        const employee = await Employee.findOne(
            {
                where: { username: req.body.username }
            }
        )
        if (!employee) return res.json({ message: 'Tên đăng nhập không chính xác' });
        bcryptjs.compare(req.body.password, employee.hash_password, (error, result) => {
            if (error) return res.send(error);
            if (result === true) {
                return res.json({
                    message: "Đăng nhập thành công",
                    token: jwt.sign({
                        id: employee.id,
                        username: employee.username
                    },
                        process.env.JWT_SECRET,
                        { expiresIn: '1d' })
                })
            }
            return res.json({ message: 'Mật khẩu không chính xác' })
        })
    } catch (error) {
        return error;
    }
};

exports.list = async (req, res) => {
    try {
        const employees = await Employee.findAndCountAll({
            where: req.conditions,
            offset: req.query.offset,
            limit: req.query.limit
        });
        return res.json(employees);
    } catch (error) {
        return error;
    }
};

exports.update = async (req, res) => {
    try {
        const { name, username, password, birthday, address, phone, gender, roles } = req.body;
        await Employee.update({
            name,
            username,
            password,
            birthday,
            address,
            phone,
            gender,
            roles
        }, {
            where: {
                id: req.params.id
            }
        });
        const employee = await Employee.findOne({
            where: { id: req.params.id }
        });
        return res.json({
            message: `Đã cập nhật employee 🚀, id: ${req.params.id}`,
            employee
        });
    } catch (error) {
        return error;
    }
};

exports.delete = async (req, res) => {
    try {
        const employee = await Employee.findOne({
            where: { id: req.params.id }
        });
        // await Employee.destroy({ where: { id: req.params.id } });
        await employee.destroy();
        return res.json({
            message: `Đã xóa employee 🚀, id: ${req.params.id}`,
            employee
        })
    } catch (error) {
        return error;
    }

};

exports.detail = async (req, res) => {
    try {
        const employee = await Employee.findOne({
            where: { id: req.params.id }
        });
        return res.json(employee);
    } catch (error) {
        return error;
    }

};