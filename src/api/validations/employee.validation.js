const Joi = require('joi');

module.exports = {
    listValidation: {
        query: {
            keyword: Joi.string()
                .allow(null, ''),
            address: Joi.string()
                .allow(null, ''),
            genders: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            roles: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .default(10)
        }
    },
    createValidation: {
        body: {
            name: Joi.string()
                .required(),
            username: Joi.string()
                .required(),
            password: Joi.string()
                .min(6)
                .required(),
            birthday: Joi.date()
                .required(),
            address: Joi.string()
                .required(),
            phone: Joi.string()
                .length(10)
                .required(),
            gender: Joi.string()
                .only(['male', 'female', 'third gender'])
                .required(),
            roles: Joi.array()
                .items(Joi.string()
                    .only(['read', 'list', 'create', 'update', 'delete'])
                )
                .required()
        }
    },
    updateValidation: {
        body: {
            name: Joi.string()
                .allow(null, ''),
            username: Joi.string()
                .allow(null, ''),
            password: Joi.string()
                .min(6)
                .allow(null, ''),
            birthday: Joi.date()
                .allow(null, ''),
            address: Joi.string()
                .allow(null, ''),
            phone: Joi.string()
                .length(10)
                .allow(null, ''),
            gender: Joi.string()
                .only(['male', 'female', 'third gender'])
                .allow(null, ''),
            roles: Joi.array()
                .items(Joi.string()
                    .only(['read', 'list', 'create', 'update', 'delete'])
                )
                .allow(null, ''),
        }
    }
};