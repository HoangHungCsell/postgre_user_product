const Joi = require('joi');

module.exports = {
    createValidation: {
        body: {
            name: Joi.string()
                .required(),
            username: Joi.string()
                .required(),
            password: Joi.string()
                .min(6)
                .required(),
            birthday: Joi.date()
                .required(),
            address: Joi.string()
                .required(),
            phone: Joi.string()
                .length(10)
                .required(),
            gender: Joi.string()
                .only(['male', 'female', 'third gender'])
                .required()
        }
    },
    updateValidation: {
        body: {
            name: Joi.string()
                .allow(null, ''),
            username: Joi.string()
                .allow(null, ''),
            password: Joi.string()
                .min(6)
                .allow(null, ''),
            birthday: Joi.date()
                .allow(null, ''),
            address: Joi.string()
                .allow(null, ''),
            phone: Joi.string()
                .length(10)
                .allow(null, ''),
            gender: Joi.string()
                .only(['male', 'female', 'third gender'])
                .allow(null, ''),
        }
    }
};