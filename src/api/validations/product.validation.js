const Joi = require('joi');


module.exports = {
    createValidation: {
        body: {
            image: Joi.array()
                .items(Joi.string())
                .required(),
            name: Joi.string()
                .required(),
            unit: Joi.string()
                .only(["Bộ", "Cái", "Đôi", "Chiếc"])
                .default("Cái"),
            description: Joi.string()
                .max(500)
                .default(null),
            price: Joi.number()
                .required(),
            discount: Joi.number()
                .default(0),
            currency: Joi.string()
                .uppercase()
                .only(["VND", "USD"])
                .default("VND"),
            categories: Joi.array()
                .items(Joi.string())
                .required()
        }
    },
    updateValidation: {
        body: {
            image: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            name: Joi.string()
                .allow(null, ''),
            unit: Joi.string()
                .only(["Bộ", "Cái", "Đôi", "Chiếc"])
                .allow(null, ''),
            description: Joi.string()
                .max(500)
                .allow(null, ''),
            price: Joi.number()
                .allow(null, ''),
            discount: Joi.number()
                .allow(null, ''),
            currency: Joi.string()
                .uppercase()
                .only(["VND", "USD"])
                .allow(null, ''),
            categories: Joi.array()
                .items(Joi.string())
                .allow(null, '')
        }
    }
};