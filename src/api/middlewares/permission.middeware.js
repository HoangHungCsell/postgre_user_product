
const Employee = require('../models/employee.model');
const Customer = require('../models/customer.model');
const Order = require('../models/order.model');
function authorizeEmp(roles) {
    return [
        async (req, res, next) => {
            const employee = await Employee.findOne({
                where: { id: req.data.id }
            });
            if (!employee) return res.json({ message: 'Tài khoản không tồn tại!' });
            const check = employee.roles.find(x => x === roles);
            if (!check) return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
            next();

        }
    ];
}

function authorizeCus(roles) {
    return [
        async (req, res, next) => {
            const customer = await Customer.findOne({
                where: { id: req.data.id }
            });
            if (!customer) return res.json({ message: 'Tài khoản không tồn tại!' });
            const check = customer.roles.find(x => x === roles);

            if (req.data.id !== parseInt(req.params.id) || !check)
                return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
            next();
        }
    ];
}

function authorizeOrd(roles) {
    return [
        async (req, res, next) => {
            const order = await Order.findOne({
                where: { id: req.params.id }
            });
            if (req.data.id !== order.created_by.id)
                return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
            next();

        }
    ];
}

module.exports = {
    authorizeEmp,
    authorizeCus,
    authorizeOrd
};