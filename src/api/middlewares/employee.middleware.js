const Sequelize = require('sequelize');
const { or, iLike } = Sequelize.Op;
exports.condition = async (req, res, next) => {
    try {
        const params = req.query ? req.query : {};
        if (params.keyword) {
            const conditions = [
                {
                    [or]: [
                        {
                            name: {
                                [iLike]: `%${params.keyword}%`
                            }
                        },
                        {
                            username: {
                                [iLike]: `%${params.keyword}%`
                            }
                        }
                    ]
                }
            ];
            req.conditions = conditions;
        }

        return next();
    } catch (error) {
        return error;
    }
};