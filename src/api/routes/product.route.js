
const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/product.controller');
const auth = require('../middlewares/login.middleware');
const { authorizeEmp } = require('../middlewares/permission.middeware');
const {
    createValidation,
    updateValidation
} = require('../validations/product.validation');

const router = express.Router();

router
    .route('/')
    .get(
        auth,
        authorizeEmp('list'),
        controller.list
    )
    .post(
        auth,
        authorizeEmp('create'),
        validate(createValidation),
        controller.create
    )
router
    .route('/:id')
    .get(
        auth,
        authorizeEmp('read'),
        controller.detail
    )
    .put(
        auth,
        authorizeEmp('update'),
        validate(updateValidation),
        controller.update
    )
    .delete(
        auth,
        authorizeEmp('delete'),
        controller.delete
    );



module.exports = router;