
const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/employee.controller');
const auth = require('../middlewares/login.middleware');
const middleware = require('../middlewares/employee.middleware');
const { authorizeEmp } = require('../middlewares/permission.middeware');
const {
    listValidation,
    createValidation,
    updateValidation
} = require('../validations/employee.validation');
const router = express.Router();

router
    .route('/')
    .get(
        auth,
        authorizeEmp('list'),
        validate(listValidation),
        middleware.condition,
        controller.list
    )
router
    .route('/:id')
    .get(
        auth,
        authorizeEmp('read'),
        controller.detail
    )
    .put(
        auth,
        authorizeEmp('update'),
        validate(updateValidation),
        controller.update
    )
    .delete(
        auth,
        authorizeEmp('delete'),
        controller.delete
    );

router
    .route('/register')
    .post(
        validate(createValidation),
        controller.register
    );

router
    .route('/login')
    .post(
        controller.login
    )


module.exports = router;