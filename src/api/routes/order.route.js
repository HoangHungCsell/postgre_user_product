
const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/order.controller');
const auth = require('../middlewares/login.middleware');
const { authorizeOrd } = require('../middlewares/permission.middeware');
const {
    createValidation,
    updateValidation
} = require('../validations/order.validation');

const router = express.Router();

router
    .route('/')
    .get(
        auth,
        controller.list
    )
    .post(
        auth,
        validate(createValidation),
        controller.create
    )
router
    .route('/:id')
    .get(
        auth,
        authorizeOrd('read'),
        controller.detail
    )
    .put(
        auth,
        authorizeOrd('update'),
        validate(updateValidation),
        controller.update
    )
    .delete(
        auth,
        authorizeOrd('delete'),
        controller.delete
    );



module.exports = router;