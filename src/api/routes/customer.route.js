
const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/customer.controller');
const auth = require('../middlewares/login.middleware');
const middeware = require('../middlewares/customer.middleware');
const { authorizeEmp, authorizeCus } = require('../middlewares/permission.middeware');
const {
    createValidation,
    updateValidation
} = require('../validations/customer.validation');
const router = express.Router();

router
    .route('/')
    .get(
        auth,
        authorizeEmp('list'),
        middeware.condition,
        controller.list
    )
router
    .route('/:id')
    .get(
        auth,
        authorizeCus('read'),
        controller.detail
    )
    .put(
        auth,
        authorizeCus('update'),
        validate(updateValidation),
        controller.update
    )
    .delete(
        auth,
        authorizeCus('delete'),
        controller.delete
    );

router
    .route('/register')
    .post(
        validate(createValidation),
        controller.register
    );

router
    .route('/login')
    .post(
        controller.login
    )


module.exports = router;