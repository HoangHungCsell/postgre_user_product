
const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize(process.env.DATABASE_URL);
// const Customer = require('../models/customer.model');

const Order = sequelize.define("Order", {
    image: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    name: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    unit: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    },
    intoMoney: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    price: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    discount: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    currency: {
        type: DataTypes.STRING,
        allowNull: false
    },
    categories: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    created_by: {
        type: DataTypes.JSONB
    }

}, {
    tableName: 'orders'
});

// Customer.hasOne(Order);
// Order.belongsTo(Customer, {
//     foreignKey: {
//         name: 'id'
//     }
// });


// (async () => {
//     await sequelize.sync({ force: true });
//     // Code here
// })();

module.exports = Order;