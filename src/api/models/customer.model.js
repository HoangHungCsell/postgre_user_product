
const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize(process.env.DATABASE_URL);
const Order = require('../models/order.model');

const Customer = sequelize.define("Customer", {
    name: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    hash_password: {
        type: DataTypes.STRING
    },
    birthday: {
        type: DataTypes.DATE,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: true
    },
    gender: {
        type: DataTypes.STRING,
        allowNull: false
    },
    roles: {
        type: DataTypes.JSONB,
        defaultValue: ['read', 'update', 'delete']
    },
    orders: {
        type: DataTypes.JSONB,
        references: {
            model: Order

        },
        defaultValue: []
    }
}, {
    tableName: 'customers'
});

// Customer.hasOne(Order, {
//     foreignKey: {
//         type: DataTypes.UUID,
//         name: 'orders'
//     }
// });
// Order.belongsTo(Customer);

// (async () => {
//     await sequelize.sync({ force: true });
//     // Code here
// })();

module.exports = Customer;