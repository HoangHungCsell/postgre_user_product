
const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize(process.env.DATABASE_URL);

const Employee = sequelize.define("Employee", {
    name: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    hash_password: {
        type: DataTypes.STRING
    },
    birthday: {
        type: DataTypes.DATE,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: true
    },
    gender: {
        type: DataTypes.STRING,
        allowNull: false
    },
    roles: {
        type: DataTypes.JSONB,
        allowNull: false
    }
}, {
    tableName: 'employees'
});

// (async () => {
//     await sequelize.sync({ force: true });
//     // Code here
// })();

module.exports = Employee;